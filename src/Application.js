import device;
import ui.StackView as StackView;

import src.TitleScreen as TitleScreen;
import src.GameScreen as GameScreen;
import src.util as util;

exports = Class(GC.Application, function () {

	this.initUI = function () {
		util.scaleRootView(this, 683, 1024);

		var titlescreen = new TitleScreen()
			gamescreen = new GameScreen();

		var rootView = new StackView({
			superview: this.view,
			x: 0,
			y: 0,
			width: 683,
			height: 1024,
			clip: true
		});


		rootView.push(titlescreen);

		titlescreen.on('titlescreen:start', function() {
			rootView.push(gamescreen);
			gamescreen.emit('game:start');
		});

		gamescreen.on('gamescreen:end', function() {
			titlescreen.titleView.setText("Score: " + gamescreen.score);
			titlescreen.promptView.setText("Touch the screen to play again");

			rootView.pop();
		});

	};

	this.launchUI = function () {};
});
