import animate;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;
import src.Gem as Gem;
import AudioManager;


var x_offset = 8,
	y_offset = 150,
	gem_margin = 5,
	board_size = 7,
	gem_size = 91,
	game_on = false,
	game_length = 30000;

exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: 320,
			height: 480
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	this.build = function () {
		var game = this;

		this.on('game:start', function() {
			startGame.call(game);
		});

		this._sound = new AudioManager({
			path: "resources/audio/",
			files: {
				effect: {
					volume: 0.8
				},
				cleargem:{
					volume: 0.2
				}
			}
		});

		this.buildSubviews()

		this._gems = this.buildMatrix();

		this.interval = 100;
		this.total_elapsed_times = [];

		this.scores = [];
		this.points_views = [];

		this.selected_gem = null;
		this.input_disabled = false;
		this.remaining_time = game_length;

		bindGemEvents.call(this);
	};

	this.buildSubviews = function() {
		var bg = new Image({ url: 'resources/images/misc/bg.png' });

		this.background = new ui.ImageView({
      		superview: this,
      		image: bg,
      		x: 0,
      		y: 0,
      		width: 800,
      		height: 1024
    	});

    	this._score_label = new ui.TextView({
			superview: this,
			x: 200,
			y: 22,
			width: 341,
			height: 100,
			size: 65,
			horizontalAlign: "center",
			wrap: false,
			color: '#FFF',
			text: 'Score: '
		});

		this._score = new ui.TextView({
			superview: this,
			x: 420,
			y: 22,
			width: 341,
			height: 100,
			size: 65,
			horizontalAlign: "center",
			wrap: false,
			color: '#FFF'
		});

		var score_bar_image = new Image({ url: "resources/images/misc/score_bar.png" });

		this._score_bar = new ui.ImageView({
			superview: this,
			image: score_bar_image,
			x: 42,
			y: 850,
			width: 600,
			height: 28,
			visible: true
		});

		var selected_image = new Image({ url: "resources/images/cursors/selected.png" });

		this.selected_view = new ui.ImageView({
			superview: this,
			image: selected_image,
			x: 0,
			y: 0,
			width: 98,
			height: 98,
			visible: false
		});
	};

	this.buildMatrix = function() {
		var gems = [];

		for (var row = 0; row < board_size; row++) {
			gems.push([]);

			for (var col = 0; col < board_size; col++) {

				//checks if there are gems that are next to each
				//other. if gems > 2, make sure that the next gem created
				//is not of forbidden type

				var forbidden_types = [];

				if (col - 2 >= 0) {
					if (gems[row][col - 1].gemtype === gems[row][col - 2].gemtype) {
						forbidden_types.push(gems[row][col - 1].gemtype);
					}
				}

				if (row - 2 >= 0) {
					if (gems[row - 1][col].gemtype === gems[row - 2][col].gemtype) {
						forbidden_types.push(gems[row - 1][col].gemtype);
					}
				}

				var gem = new Gem({ row: row, col: col, forbidden_types: forbidden_types });

				//the actual x,y position of the gems
				gem.style.y = y_offset + row * (gem.style.height + gem_margin);
				gem.style.x = x_offset + col * (gem.style.width + gem_margin);

				this.addSubview(gem);

				gems[row].push(gem);
			}
		}

		return gems;
	};
});

function startGame() {
	var game = this;

	this.score = 0;
	this.score_multiplier = 1;
	this.cleared_gems = 0;

	this.game_length = game_length;
	this.game_time = this.game_length;
	this.total_elapsed_time = 0;

	this.game_timer = setInterval(function() {
		tick.call(game);
	}, this.interval);
}

function tick() {
	if (this.game_time > 0) {
		var time_ratio = this.game_time / this.game_length;

		if (this.game_time > this.game_length){
			this.game_time = this.game_length;
		}

		this.game_time -= 100;

		this.total_elapsed_time += this.interval;

		this._score_bar.style.width = time_ratio * 600;
		this._score.setText(this.score);
	} else {
		endGame.call(this);
	}
}

function endGame() {
	clearInterval(this.game_timer);
	this.game_time = this.game_length;
	this.emit('gamescreen:end');
}

function bindGemEvents() {
	var that = this;

	this._gems.forEach(function(row) {
		row.forEach(function(gem) {
			gem.on('gem:clicked', function() {
				handleGemClick.call(that, gem);
			});
		});
	});
}

function bindSomeGemEvents(gems_to_bind) {
	var that = this;

	gems_to_bind.forEach(function(gem) {
		gem.on('gem:clicked', function() {
			handleGemClick.call(that, gem);
		});
	});
}

function handleGemClick(gem) {
	var game = this;

	if (this.input_disabled === false) {
		if (this.selected_gem !== null || !this.selected_view.style.visible === false) {
			diff_y = gem.row - this.selected_gem.row,
			diff_x = gem.col - this.selected_gem.col;

			if (gem === this.selected_gem) {
				deselectGem.call(this);
			} else if (Math.abs(diff_x) > 1 || Math.abs(diff_y) > 1 || (Math.abs(diff_x) === 1 && Math.abs(diff_y) === 1)) {
				selectGem.call(this, gem);
			} else {
				attemptMove.call(this, gem);
			}

		} else {
			selectGem.call(this, gem);
			this._sound.play("effect", {loop: false});
		}
	}
}

function deselectGem() {
	this.selected_gem = null;
	this.selected_view.style.visible = false;
}

function selectGem(gem) {
	this.selected_gem = gem;
	this.selected_view.style.y = y_offset + this.selected_gem.row * (this.selected_gem.style.height + gem_margin) - 4;
	this.selected_view.style.x = x_offset + this.selected_gem.col * (this.selected_gem.style.width + gem_margin) - 4;
	this.selected_view.style.visible = true;
}

function attemptMove(gem) {
	var game = this;

	var mov_y = gem.row - this.selected_gem.row,
		mov_x = gem.col - this.selected_gem.col,
		move = [mov_y, mov_x];

	var valid_move = validMove.call(this, this.selected_gem, move);
	if (valid_move === false) { valid_move = validMove.call(this, gem, [-mov_y, -mov_x]); }

	if (valid_move === true) {
		performMove.call(this, this.selected_gem, gem);
		var gems_to_destroy = findGemStreaks.call(this);
		clearGems.call(this, gems_to_destroy);

		this.input_disabled = true;
		this.selected_gem = null;
		this.selected_view.style.visible = false;
	} else {
		this.selected_gem = null;
		this.selected_view.style.visible = false;
	}
}

function findGemStreaks() {
	var gems = this._gems;
	var gems_to_destroy = [];
	var row_index = 0;

	//search through rows, grabbing horizontal streaks 3 or longer
	gems.forEach(function(row) {
		var col_index = 0;
		var consecutive = 0;

		row.forEach(function(gem) {
			if (col_index > 0) {
				if (gem.gemtype === gems[row_index][col_index - 1].gemtype) {
					consecutive++;

					if (consecutive === 2) {
						gems_to_destroy.push(gems[row_index][col_index - 1]);
						gems_to_destroy.push(gems[row_index][col_index - 2]);
						gems_to_destroy.push(gem);
					} else if (consecutive > 2) {
						gems_to_destroy.push(gem);
					}
				} else {
					consecutive = 0;
				}
			}
			col_index++;
		});

		row_index++;
	});

	//search through columns, grabbing vertical streaks 3 or longer
	//(and only including gems not already added above)
	for (var col = 0; col < board_size; col++) {
		var consecutive = 0;

		for (var row = 0; row < board_size; row++) {
			var gem = gems[row][col];

			if (row > 0) {
				if (gem.gemtype === gems[row - 1][col].gemtype) {
					consecutive++;

					if (consecutive === 2) {
						if (gems_to_destroy.indexOf(gems[row - 1][col]) === -1) {
							gems_to_destroy.push(gems[row - 1][col]);
						}
						if (gems_to_destroy.indexOf(gems[row - 2][col]) === -1) {
							gems_to_destroy.push(gems[row - 2][col]);
						}
						if (gems_to_destroy.indexOf(gem) === -1) {
							gems_to_destroy.push(gem);
						}
					} else if (consecutive > 2) {
						if (gems_to_destroy.indexOf(gem) === -1) {
							gems_to_destroy.push(gem);
						}
					}
				} else {
					consecutive = 0;
				}
			}
		}
	}

	return gems_to_destroy;
}

function clearGems(gems_to_destroy) {
	var game = this;

	this._sound.play("cleargem", {loop: false});

	this.cleared_gems += gems_to_destroy.length;

	if (gems_to_destroy.length === 3) {
		this.score_multiplier = 1;
	} else if (gems_to_destroy.length > 3) {
		this.score_multiplier += gems_to_destroy.length - 3;
	}

	//increase the game time if more gems gets cleared
	var points = this.score_multiplier * gems_to_destroy.length * 10;
	this.score += points;
	this.game_time += Math.sqrt(this.score_multiplier) * gems_to_destroy.length * 250;

	if (this.game_length > 10000) {
		this.game_length = game_length - this.total_elapsed_time / 10;
	}


	if (points !== 0) {
		var points_view = new ui.TextView({
			superview: this,
			x: parseInt(-150 + 300 * Math.random()),
			y: 900,
			width: 576,
			height: 150,
			horizontalAlign: "center",
			size: points,
			text: "+" + points,
			color: "yellow"
		});

		this.addSubview(points_view);

		var points_animation = setInterval(function() {
			points_view.style.y -= 4.5;
		}, 50);

		setTimeout(function() {
			game.removeSubview(points_view);
			clearInterval(points_animation);
		}, 1000);
	}

	if (gems_to_destroy.length > 0) {
		var game = this;
		var num_timers_finished = 0;

		gems_to_destroy.forEach(function(gem) {
			var ticker = 0;

			var shrink_animation = setInterval(function() {
				ticker++;

				if (ticker < 10) {
					gem.gem.style.width -= 20;
					gem.gem.style.height -= 20;
					gem.gem.style.x += 10;
					gem.gem.style.y += 10;
				} else {
					game._gems[gem.row][gem.col] = null;
					game.removeSubview(gem);

					num_timers_finished++;
					clearInterval(shrink_animation);
				}
			}, 20)
		});

		var animation_wait_timer = setInterval(function() {
			if (num_timers_finished === gems_to_destroy.length) {
				dropExisting.call(game);
				clearInterval(animation_wait_timer);
			}
		}, 30);
	} else {
		this.input_disabled = false;
	}
}

function dropExisting() {
	var game = this;
	var gems = this._gems;

	for (var row = board_size - 2; row > -1; row--) {
		for (var col = 0; col < board_size; col++) {
			if (gems[row][col] !== null) {
				var gem = gems[row][col];

				var step_down = 1;

				while (row + step_down < board_size && gems[row + step_down][col] === null) {
					performSingleMove.call(game, gem, [row + step_down, col]);

					step_down++;
				}
			}
		}
	}

	dropNew.call(this);
}

function dropNew() {
	var game = this;
	var gems = this._gems;
	var new_gems = [];


	for (var col = 0; col < board_size; col++) {
		var num_gems;

		for (var row = board_size - 1; row > -1; row--) {
			if (gems[row][col] === null) {
				num_gems = row + 1;

				for (var i = row; i > -1; i--) {
					var gem = new Gem({ row: i, col: col, forbidden_types: [] });

					gem.style.y = y_offset + i * (gem.style.height + gem_margin);
					gem.style.x = x_offset + col * (gem.style.width + gem_margin);

					gems[i][col] = gem;
					new_gems.push(gem);

					game.addSubview(gem);

					gem.fallIn(y_offset + i * (gem.style.height + gem_margin));
				}
			}
		}
	}

	bindSomeGemEvents.call(game, new_gems);

	setTimeout(function() {
		clearGems.call(game, findGemStreaks.call(game));
	}, 250);
}

function performMove(selected_gem, affected_gem) {
	this._gems[selected_gem.row][selected_gem.col] = affected_gem;
	this._gems[affected_gem.row][affected_gem.col] = selected_gem;

	var temp_row = selected_gem.row;
	var temp_col = selected_gem.col;
	var temp_y = selected_gem.style.y;
	var temp_x = selected_gem.style.x;

	selected_gem.row = affected_gem.row;
	selected_gem.col = affected_gem.col;
	selected_gem.style.y = affected_gem.style.y;
	selected_gem.style.x = affected_gem.style.x;

	affected_gem.row = temp_row;
	affected_gem.col = temp_col;
	affected_gem.style.y = temp_y;
	affected_gem.style.x = temp_x;
}

function performSingleMove(gem, destination) {
	this._gems[gem.row][gem.col] = null;
	this._gems[destination[0]][destination[1]] = gem;
	gem.row = destination[0];
	gem.col = destination[1];

	var diff_y = y_offset + gem.row * (gem.style.height + gem_margin) - gem.style.y;

	gem.style.y = y_offset + gem.row * (gem.style.height + gem_margin);
	gem.style.x = x_offset + gem.col * (gem.style.width + gem_margin);

	gem.fallIn(diff_y);
}

function validMove(gem, move) {

	var gs = this._gems;
	var s_row = gem.row;
	var s_col = gem.col;
	var gtype = gem.gemtype;

	if (move[1] === 0) {
		if (s_row + 3 * move[0] < board_size && s_row + 3 * move[0] > -1) {
			if (gtype === gs[s_row + 2 * move[0]][s_col].gemtype && gtype === gs[s_row + 3 * move[0]][s_col].gemtype) {
				return true;
			}
		}
		if (s_col + 2 < board_size) {
			if (gtype === gs[s_row + move[0]][s_col + 1].gemtype && gtype === gs[s_row + move[0]][s_col + 2].gemtype) {
				return true;
			}
		}
		if (s_col - 2 > -1) {
			if (gtype === gs[s_row + move[0]][s_col - 1].gemtype && gtype === gs[s_row + move[0]][s_col - 2].gemtype) {
				return true;
			}
		}
		if (s_col + 1 < board_size && s_col - 1 > -1) {
			if (gtype === gs[s_row + move[0]][s_col - 1].gemtype && gtype === gs[s_row + move[0]][s_col + 1].gemtype) {
				return true;
			}
		}
		return false;
	} else {
		if (s_col + 3 * move[1] < board_size && s_col + 3 * move[1] > -1) {
			if (gtype === gs[s_row][s_col + 2 * move[1]].gemtype && gtype === gs[s_row][s_col + 3 * move[1]].gemtype) {
				return true;
			}
		}
		if (s_row + 2 < board_size) {
			if (gtype === gs[s_row + 1][s_col + move[1]].gemtype && gtype === gs[s_row + 2][s_col + move[1]].gemtype) {
				return true;
			}
		}
		if (s_row - 2 > -1) {
			if (gtype === gs[s_row - 1][s_col + move[1]].gemtype && gtype === gs[s_row - 2][s_col + move[1]].gemtype) {
				return true;
			}
		}
		if (s_row + 1 < board_size && s_row - 1 > -1) {
			if (gtype === gs[s_row - 1][s_col + move[1]].gemtype && gtype === gs[s_row + 1][s_col + move[1]].gemtype) {
				return true;
			}
		}
		return false;
	}
}