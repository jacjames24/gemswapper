import ui.View;
import ui.TextView;
import ui.ImageView;
import ui.resource.Image as Image;
import AudioManager;


var path = 'resources/images/misc/';
var bg = new Image({ url: path + 'bg.png' });
var scrollImg = new Image({ url: path + 'scroll.png' });

exports = Class(ui.View, function(supr) {
	this.init = function(opts) {
		opts = merge(opts, {
			width: 576,
			height: 1024
		});

		supr(this, 'init', [opts]);

		this.addBGM()
		this.addBg();
		this.addScroll();
		this.addTitle();
		this.addPrompt()
		this.addStartLayer();
	};

	this.addBGM = function(){
		this._sound = new AudioManager({
			path: 'resources/audio/',
			files: {
				bg:{
					volume: 0.8,
					loop: true,
					background: true
				}
			}
		});
		this._sound.play("bg", {loop: true});
	};

	this.addBg = function() {
		this.background = new ui.ImageView({
      	superview: this,
      	image: bg,
      	x: 0,
      	y: 0,
      	width: 800,
      	height: 1024
    	});
	};

	this.addScroll = function() {
		this.scroll = new ui.ImageView({
			superview: this,
      		image: scrollImg,
      		x: 0,
      		y: 150,
      		width: 670,
      		height: 500
    	});
	}

	this.addTitle = function() {
		this.titleView = new ui.TextView({
			superview: this,
			x: 50,
			y: 260,
			width: 576,
			height: 200,
			size: 200,
			text: "Orbs of Light",
			color: "#000",
			autoSize: false
		});
	};

	this.addPrompt = function() {
		this.promptView = new ui.TextView({
			superview: this,
			x: 43,
			y: 380,
			width: 576,
			height: 200,
			size: 35,
			text: "Touch the screen to play",
			color: "#000"
		});
	};


	this.addStartLayer = function() {
		this.startLayer = new ui.View({
			superview: this,
			x: 0,
			y: 0,
			width: 576,
			height: 1024
		});

		this.startLayer.on('InputSelect', bind(this, function() {
			this.emit('titlescreen:start');
		}));
	};
});