## This is a sample game Match-3 type game using GameClosure Devkit.##

Some screenshots

![Screen Shot 2016-10-19 at 15.28.44.png](https://bitbucket.org/repo/qbyXBe/images/2821883366-Screen%20Shot%202016-10-19%20at%2015.28.44.png)

![Screen Shot 2016-10-19 at 15.29.05.png](https://bitbucket.org/repo/qbyXBe/images/3738710611-Screen%20Shot%202016-10-19%20at%2015.29.05.png)

![Screen Shot 2016-10-19 at 15.29.32.png](https://bitbucket.org/repo/qbyXBe/images/1199400702-Screen%20Shot%202016-10-19%20at%2015.29.32.png)

### Prerequisite ###
Game Closure's framework which you can download here: http://docs.gameclosure.com/guide/install.html

After that, proceed to install the game. 

### How to install the game and run ###
1. Clone the repo  `git clone https://jacjames24@bitbucket.org/jacjames24/gemswapper.git gemswapper`
2. Go to the gemswapper folder  `cd gemswapper`
3. Initialize GC devkit within project folder (ignore the daunting npm ERR) `devkit install`
4. Start the simulator by typing:  `devkit serve`
 which will output: [serve]  serving at http://localhost:9200/ and http://192.168.100.21:9200/
5. Go to the browser, and go to either of the two URLs
6. Click gemswapper.
7. Click simulate
8. Play the game (oh, make sure that the simulator is not muted:> )

### What if I can't still play it because apparently when I clone it is still incomplete? ###

I've uploaded the whole 100MB++ game (including the GC submodule) in case something goes wrong with the command line installation.

Go to this URL: https://drive.google.com/file/d/0B6AnnzaYS55aYWZNa1JSZEFWcEE/view?usp=sharing

devkit-application-template
===========================

Initial file structure for Gameclosure Devkit applications.

Includes an empty folder structure that matches the devkit defaults
and a minimal Application.js file with a "Hello, world!" text view.


You can specify a different initial template for new devkit applications
by adding the `--git-template` paramater to `devkit init` and passing in
a path to a git repository.

~~~
devkit init newproject --git-template https://github.com/gameclosure/devkit-application-template
~~~